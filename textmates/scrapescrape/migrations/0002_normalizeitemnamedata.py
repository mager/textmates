# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrapescrape', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='NormalizeItemNameData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('item_name', models.CharField(max_length=200)),
                ('normalized_item_name', models.CharField(max_length=200)),
            ],
        ),
    ]
