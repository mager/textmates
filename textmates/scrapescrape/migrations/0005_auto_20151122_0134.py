# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrapescrape', '0004_normalizeitemnamedata_normalized_data'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='NormalizeItemNameData',
            new_name='NormalizedItemNameData',
        ),
    ]
