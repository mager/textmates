import json
import urllib2
from lxml import html
import requests

from django_twilio.decorators import twilio_view
from twilio.twiml import Response

from sms.models import TextData
from scrapescrape import models

@twilio_view
def sms(request):
    r = Response()
    body_text = request.POST.get('Body', '')
    from_number = request.POST.get('From', '')
    t = TextData()
    t.from_number = from_number
    t.text_data = body_text
    t.save()
    print "Querrying for %s" % (t.text_data)
    item = search_for_item(t.text_data)
    msg = ''
    if item is None:
        msg = "Sorry, nothing was found. Womp womp"
    else:
        msg = "Ordering from %s at %s" % (item.rest_name, item.pickup_address)
    r.message(msg)
    return r

def search_for_item(text):
    text = text.strip()
    search_term = text.lower()
    try:
        normalized_item = models.NormalizedItemNameData.objects.get(item_name=search_term)
        scraped_data = normalized_item.normalized_data
        return scraped_data
    except:
        # Not found in our database, fetch from Sosh
        return fetch_from_sosh(text)

def fetch_from_sosh(text):
    '''
    We didn't find something to deliver, so lets check on Sosh
    :param text:
    :return:
    '''
    SOSH_ENDPOINT = "http://sosh.com/search-api/1/web/?q="
    query = SOSH_ENDPOINT + text
    #response = urllib2.urlopen(query).read()
    data = json.load(urllib2.urlopen(query))
    if len(data['results']) == 0:
        return None
    top_result = data['results'][0]
    rest_name = top_result['data']['subtitle']
    top_result_url = "http://sosh.com" + top_result['data']['ticketing_url']
    page = requests.get(top_result_url)
    tree = html.fromstring(page.content)
    from_address = tree.xpath('//div[@id="address"]/text()')
    sd = models.ScrapedData()
    sd.item_name = text
    sd.pickup_address = from_address[0]
    sd.rest_name = rest_name
    sd.save()
    nind = models.NormalizedItemNameData()
    nind.item_name = text
    nind.normalized_data = sd
    nind.save()
    return sd
