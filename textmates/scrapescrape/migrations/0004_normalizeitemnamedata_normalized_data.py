# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrapescrape', '0003_remove_normalizeitemnamedata_normalized_item_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='normalizeitemnamedata',
            name='normalized_data',
            field=models.ForeignKey(default=1, to='scrapescrape.ScrapedData'),
        ),
    ]
