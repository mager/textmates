# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrapescrape', '0002_normalizeitemnamedata'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='normalizeitemnamedata',
            name='normalized_item_name',
        ),
    ]
