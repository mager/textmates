# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrapescrape', '0005_auto_20151122_0134'),
    ]

    operations = [
        migrations.AddField(
            model_name='scrapeddata',
            name='rest_name',
            field=models.CharField(default='MagerAteMe', max_length=200),
            preserve_default=False,
        ),
    ]
