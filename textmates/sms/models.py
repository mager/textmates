from django.db import models

# Create your models here.

class TextData(models.Model):
    # From Number
    from_number = models.CharField(max_length=20)
    text_data = models.CharField(max_length=120)
