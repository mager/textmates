from django.db import models
from jsonfield import JSONField

# Create your models here.

class ScrapedData(models.Model):
    # Name of the item that people are searching for, example pizza
    item_name = models.CharField(max_length=200)
    # Scraped address
    pickup_address = models.CharField(max_length=200)
    # Extra info
    meta_information = JSONField()
    rest_name = models.CharField(max_length=200)

def get_first_dat():
    return ScrapedData.objects.get(id=1)

class NormalizedItemNameData(models.Model):
    # Different variations of what people might search for
    item_name = models.CharField(max_length=200)
    # Normalized name
    normalized_data = models.ForeignKey(ScrapedData, default=1)
