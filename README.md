# Textmates

An SMS interface for Postmates.

## Instructions

1. Text something that you want to (267) 713-3663.
2. You should get a text with an address.
3. Go there and enjoy.

![Our app right now](http://i.imgur.com/xWleGyh.jpg)

*This is a work in progress. We'll make it way better.*
